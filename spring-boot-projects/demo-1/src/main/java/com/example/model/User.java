package com.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="test_data")
public class User {
	
	@Id
	int ad_id;
	String ad_name;
	String ad_salary;
	public int getAd_id() {
		return ad_id;
	}
	public void setAd_id(int ad_id) {
		this.ad_id = ad_id;
	}
	public String getAd_name() {
		return ad_name;
	}
	public void setAd_name(String ad_name) {
		this.ad_name = ad_name;
	}
	public String getAd_salary() {
		return ad_salary;
	}
	public void setAd_salary(String ad_salary) {
		this.ad_salary = ad_salary;
	}
	public User() {
		
	}
	public User(int ad_id, String ad_name, String ad_salary) {
		super();
		this.ad_id = ad_id;
		this.ad_name = ad_name;
		this.ad_salary = ad_salary;
	}


	
}
