package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.UserDao;
import com.example.model.User;

@RestController
@RequestMapping("/")
public class dummyController {
	
	@Autowired
	UserDao userDao;
	
	@RequestMapping("/test")
	public String test() {
		return "test";
	}
	
	@RequestMapping("/getusers")
	public List<User> getAll(){
		
		
		return userDao.findAll(); 
	}
	
	@PostMapping("/persist")
	public List<User> persist(@RequestBody User user){
		userDao.save(user);
		return (List<User>) userDao.findAll();
	}
	@RequestMapping("/home")
	public String home_page() {
		return "home.jsp";
	}
}
