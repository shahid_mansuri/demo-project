package com.microsoft.google.Google;


import java.sql.Timestamp;
import java.util.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoogleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoogleApplication.class, args);
		
		System.out.println("Project started----------------------");
		
		System.out.println("Date"+ new Date());
		System.out.println("Timestamp"+ new Timestamp(new Date().getTime()));
		
	}

}
