package com.microsoft.google.Google.Model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

@Entity(name="user_tb")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String username;
	private String fname;
	private String lname;
	private String email;
	private String file_path;
	private Timestamp time;
	private String operation;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	
	public Timestamp getTime() {
		return time;
	}
	public void setTime(Timestamp time) {
		this.time = time;
	}
	public String getOperation(){
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public User() {
		
	}

	public User(Long id, String email, String file_path, String fname, String lname, String username) {
		super();
		this.id = id;
		this.email = email;
		this.file_path = file_path;
		this.fname = fname;
		this.lname = lname;
		this.username = username;
		
	}
	
	@PrePersist
    public void onPrePersist() {
        audit("INSERT");
    }
     
    @PreUpdate
    public void onPreUpdate() {
        audit("UPDATE");
    }
     
    @PreRemove
    public void onPreRemove() {
        audit("DELETE");
    }
    
    private void audit(String operation) {
    
    	Date date = new Date();  
        Timestamp ts=new Timestamp(date.getTime()); 
    	long stamp = new Date().getTime();
    	System.out.println(ts);
        setOperation(operation);
        setTime(ts);
    }
	
}
