package com.microsoft.google.Google.Service;

import java.util.List;

import com.microsoft.google.Google.Model.User;

public interface UserService {

	void addData(User user);
	List<User> getUser();
	void sendMail(String email);
	
}
