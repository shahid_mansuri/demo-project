package com.microsoft.google.Google.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microsoft.google.Google.Model.User;
import com.microsoft.google.Google.Repos.UserRepo;
import com.microsoft.google.Google.Service.UserService;
import com.microsoft.google.Google.Service.UtilityService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepo userRepoImpl; 
	
	@Autowired
	private UtilityService util;
	
	@Override
	public void addData(User user) {
		user.setFname(util.getEmailDomain(user.getEmail()));
		userRepoImpl.addData(user);
		
	}

	@Override
	public List<User> getUser() {
		
		
		List<User> newList = new ArrayList<User>();
		newList = userRepoImpl.getUser();
		List<User> finalList = new ArrayList<User>();
		finalList =newList.stream().filter( p -> p.getId() != 1 ).collect(Collectors.toList());
		System.out.println(newList);
		return finalList;
	}

	@Override
	public void sendMail(String email) {
		
		String recipient = email;
		String sender = "mansurishahid37@gmail.com";
		
		String username ="mansurishahid37@gmail.com";
		String password="";
		
		String host = "127.0.0.1:8080";
		Properties prop = System.getProperties();
		prop.setProperty("mail.smtp.host", host);
		prop.put("mail.smtp.auth",true);
		prop.put("mail.smtp.starttls.enable",true);
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");
		
		Session session = Session.getInstance(prop, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		
		try 
	      { 
	          
	         MimeMessage message = new MimeMessage(session); 

	         message.setFrom(new InternetAddress(sender)); 

	         message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient)); 
       
	         message.setSubject("This is Suject"); 

	         message.setText("This is a test mail"); 

	         Transport.send(message); 
	         System.out.println("Mail successfully sent"); 
	      } 
	      catch (MessagingException mex)  
	      { 
	         mex.printStackTrace(); 
	      } 
		
		
		
	}

}
