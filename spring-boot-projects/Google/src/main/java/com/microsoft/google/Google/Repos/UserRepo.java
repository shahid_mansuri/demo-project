package com.microsoft.google.Google.Repos;

import java.util.List;

import com.microsoft.google.Google.Model.User;

public interface UserRepo {

	void addData(User user);
	List<User> getUser();
	
}
