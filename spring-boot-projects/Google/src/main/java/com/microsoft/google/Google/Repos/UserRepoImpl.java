package com.microsoft.google.Google.Repos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.microsoft.google.Google.Model.User;

@Repository
public class UserRepoImpl implements UserRepo {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public void addData(User user) {

		System.out.println("save user ----->");

		entityManager.persist(user);

	}

	@Override
	public List<User> getUser() {

		Query query = entityManager.createNativeQuery("SELECT * from user_tb");

		List<User> userList = query.getResultList();

		return userList;
	}

}
