package com.task.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task.ServiceImpl.TempServiceImpl;

@RestController
public class TempController {

	@Autowired
	private TempServiceImpl tempServiceImpl;
	
	@GetMapping("/getData")
	public void getJsonData() {
		
		tempServiceImpl.jsonObj();
		
	}
	
	@GetMapping("/getArrayData")
	public void getJsonArray() {
		
		tempServiceImpl.getJsonArrayData();
		
	}
	
}
