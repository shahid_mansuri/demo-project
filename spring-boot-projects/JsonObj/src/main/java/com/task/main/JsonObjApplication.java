package com.task.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.task.")
public class JsonObjApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonObjApplication.class, args);
	}

}
