package com.task.ServiceImpl;

import java.io.StringWriter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.task.Service.TempService;

@Service
public class TempServiceImpl implements TempService{

	@Override
	public void jsonObj() {
		
		JSONObject manufacturerInner = new JSONObject();
		manufacturerInner.put("type", "String");
		manufacturerInner.put("required", new Boolean(true));
		
		JSONObject year = new JSONObject();
		year.put("type", "integer");
		year.put("required", new Boolean(true));
		year.put("minimum", new Integer(2011));
		year.put("maximum", new Integer(2012));
		
		JSONObject commonVal = new JSONObject();
		commonVal.put("type", "integer");
		commonVal.put("required", new Boolean(true));
		
		JSONObject commonVal1 = new JSONObject();
		commonVal1.put("type", "integer");
		commonVal1.put("required", "true");
		
		JSONObject horsepowerVal = new JSONObject();
		horsepowerVal.put("type", "integer");
		horsepowerVal.put("required", new Boolean(true));
		
		JSONObject engineInner  = new JSONObject();
		engineInner.put("cylinders", commonVal1);
		engineInner.put("displacement", commonVal1);
		engineInner.put("horsepower", commonVal);
		engineInner.put("torque", commonVal);
		
		JSONObject engine = new JSONObject();
		engine.put("type","string");
		engine.put("required", new Boolean(true));
		engine.put("properties", engineInner);
		
		JSONObject manufacturer = new JSONObject();
		manufacturer.put("manufacturer",manufacturerInner);
		manufacturer.put("model", manufacturerInner);
		manufacturer.put("year", year);
		manufacturer.put("color",manufacturerInner);
		manufacturer.put("engine",engine);
		
		JSONObject carObj = new JSONObject();
		carObj.put("description","Car");
		carObj.put("type", "object");
		carObj.put("properties",manufacturer);
		
		StringWriter out = new StringWriter();
		carObj.write(out);
		
		String jsonText = out.toString();
		System.out.println(jsonText);
		
	}

	@Override
	public void getJsonArrayData() {
		
		JSONObject manufacturerInner = new JSONObject();
		manufacturerInner.put("type", "String");
		manufacturerInner.put("required", new Boolean(true));
		
		JSONObject year = new JSONObject();
		year.put("type", "integer");
		year.put("required", new Boolean(true));
		year.put("minimum", new Integer(2011));
		year.put("maximum", new Integer(2012));
		
		JSONObject commonVal = new JSONObject();
		commonVal.put("type", "integer");
		commonVal.put("required", new Boolean(true));
		
		JSONObject commonVal1 = new JSONObject();
		commonVal1.put("type", "integer");
		commonVal1.put("required", "true");
		
		JSONObject horsepowerVal = new JSONObject();
		horsepowerVal.put("type", "integer");
		horsepowerVal.put("required", new Boolean(true));
		
		JSONObject engineInner  = new JSONObject();
		engineInner.put("cylinders", commonVal1);
		engineInner.put("displacement", commonVal1);
		engineInner.put("horsepower", commonVal);
		engineInner.put("torque", commonVal);
		
		JSONObject engine = new JSONObject();
		engine.put("type","string");
		engine.put("required", new Boolean(true));
		engine.put("properties", engineInner);
		
		JSONObject manufacturer = new JSONObject();
		manufacturer.put("manufacturer",manufacturerInner);
		manufacturer.put("model", manufacturerInner);
		manufacturer.put("year", year);
		manufacturer.put("color",manufacturerInner);
		manufacturer.put("engine",engine);
		
		JSONObject carObj = new JSONObject();
		carObj.put("description","Car");
		carObj.put("type", "object");
		carObj.put("properties",manufacturer);
		
		JSONObject carObj1 = new JSONObject();
		carObj1.put("description","Car");
		carObj1.put("type", "object");
		carObj1.put("properties",manufacturer);
		
		JSONObject carObj2 = new JSONObject();
		carObj2.put("description","Car");
		carObj2.put("type", "object");
		carObj2.put("properties",manufacturer);
		
		
		
		JSONArray dataArray = new JSONArray();
		dataArray.put(carObj);
		dataArray.put(carObj1);
		dataArray.put(carObj2);
		
		StringWriter out = new StringWriter();
		dataArray.write(out);
		
		String jsonText = out.toString();
		System.out.println(jsonText);
		
	}

}
