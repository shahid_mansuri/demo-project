package com.task.connectivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.test.Controller.UserController;

//@EntityScan(basePackages = "com.test.Model")
//@EnableJpaRepositories(basePackages = "com.test.repos")
//@SpringBootApplication(scanBasePackages = "com.test.")
@ComponentScan(basePackages ="com.test.")
@SpringBootApplication
public class DatabaseConnectivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseConnectivityApplication.class, args);
		System.out.println("main started ...............");
	}

}
