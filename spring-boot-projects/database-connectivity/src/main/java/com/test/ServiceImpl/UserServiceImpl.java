package com.test.ServiceImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.test.Dao.UserDao;
import com.test.Model.Product;
import com.test.Model.User;
import com.test.Service.UserService;
import com.test.repos.UserRepo;

@Service
public class UserServiceImpl implements UserService {

	
	
	@Autowired
	UserRepo userRepo;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Override
	public void addUser(User user) {

//		EntityManager entityManager = entityManagerFactory.createEntityManager();
//		entityManager.getTransaction().begin();
//		entityManager.persist(user);
//		entityManager.getTransaction().commit();

		System.out.println("service ----->");
		userRepo.addData(user);
	

	}

	@Override
	public void addFile(MultipartFile file) {

		String path = "D:\\task-file-upload\\";
		String new_path = "";
		new_path = path + file.getOriginalFilename();

		try {

			byte[] bytes = file.getBytes();
			Path path1 = Paths.get(new_path);
			Files.write(path1, bytes);

		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Product> filterExample() {
		
		 List<Product> productsList = new ArrayList<Product>();  
	        //Adding Products  
	        productsList.add(new Product(1,"HP Laptop",25000f));  
	        productsList.add(new Product(2,"Dell Laptop",30000f));  
	        productsList.add(new Product(3,"Lenevo Laptop",28000f));  
	        productsList.add(new Product(4,"Sony Laptop",28000f));  
	        productsList.add(new Product(5,"Apple Laptop",90000f));  
	        
	        
	        List<Product> finalList= new ArrayList<Product>();
	        finalList = productsList.stream()
	        		.filter(p -> p.getId() != 3)
	        		.collect(Collectors.toList());
		
		return finalList;
	}

	@Override
	public void updateUser(User user) {
		
		String new_name = "shahid9yahoo.com";
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		EntityTransaction updateTransaction = entityManager.getTransaction();
		updateTransaction.begin();
		
		Query query = entityManager.createQuery("UPDATE user_tb SET email = :p WHERE id = 1");
		query.setParameter("p", new_name);
		int update = query.executeUpdate();
		updateTransaction.commit();

		
	}

	@Override
	public void deleteUser(String name) {
		
		Long id = (long) 3;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		User getOldUser = entityManager.find(User.class, id);
		entityManager.getTransaction().begin();

		entityManager.remove(getOldUser);

		entityManager.getTransaction().commit();
	}

	@Override
	public List<User> readData() {
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		Query query = entityManager.createNativeQuery("SELECT * from user_tb");
		
		List<User> userList = query.getResultList();
		
		return userList;
	}
	

}
