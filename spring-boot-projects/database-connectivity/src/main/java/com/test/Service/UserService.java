package com.test.Service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.test.Model.Product;
import com.test.Model.User;

public interface UserService {

	void addUser(User user);
	void addFile(MultipartFile file);
	List<Product> filterExample();
	void updateUser(User user);
	void deleteUser(String name);
	List<User> readData();
	
}
