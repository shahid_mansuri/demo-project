package com.test.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.Model.User;


@Repository
public interface UserDao extends JpaRepository<User, Long>{
	
	

}
