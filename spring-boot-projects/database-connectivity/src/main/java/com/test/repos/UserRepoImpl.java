package com.test.repos;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.Model.User;

@Repository
public class UserRepoImpl implements UserRepo {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void addData(User user) {

		System.out.println("save user ----->");
		entityManager.persist(user);
		
	}

}
