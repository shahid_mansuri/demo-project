package com.test.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import com.test.Model.Product;
import com.test.Model.User;
import com.test.Service.UserService;



@RestController()
public class UserController {
	
	@Autowired
	UserService userServiceImpl;
	
	@GetMapping("/testUrl")
	public String testMethod() {
		
		return "hi";
	}
	
	@PostMapping("/addUser")
	public void addUser(@RequestBody User user) {
		System.out.println("controller ----->");
		userServiceImpl.addUser(user);
	
	}

	@PostMapping("/addFile")
	public void addFile(@RequestParam("file")MultipartFile file) {
		
		userServiceImpl.addFile(file);
		
	}
	

	@GetMapping("/getFilter")
	public List<Product> getFilter() {

		return userServiceImpl.filterExample();
		
	}
	
	
	@PutMapping("/updateUser")
	public void updateUser(@RequestBody User user) {
		
		userServiceImpl.updateUser(user);
		
	}
	
	@PostMapping("/deleteUser/{name}")
	public void deleteUser(@PathVariable("name")String name) {
		userServiceImpl.deleteUser(name);
	}
	
	
	@GetMapping("/getUserList")
	public List<User> getUserList(){
		
		
		return userServiceImpl.readData();
	}
}
