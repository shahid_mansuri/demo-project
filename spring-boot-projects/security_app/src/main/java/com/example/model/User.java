package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name="user")
public class User {
	@Id
	@GeneratedValue
	public int us_id;
	@Column(name="us_username")
	public String username;
	@Column(name="us_password")
	public String password;
	public int getUs_id() {
		return us_id;
	}
	public void setUs_id(int us_id) {
		this.us_id = us_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User(int us_id, String username, String password) {
		super();
		this.us_id = us_id;
		this.username = username;
		this.password = password;
	}
	
	public User() {
		
	}

}
