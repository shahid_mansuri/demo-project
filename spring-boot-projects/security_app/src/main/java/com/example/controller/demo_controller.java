package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.dao.UserRepo;
import com.example.model.AuthReq;
import com.example.model.User;
import com.example.security.JwtUtil;


@RestController
public class demo_controller {
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	JwtUtil jwtUtil;
	
	@Autowired
	private AuthenticationManager authManager;

	@GetMapping("/")
	public String home() {
		
		return "home_api";
	}
	 
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}
	
	
	@GetMapping("/admin")
	public String admin() {
		return "admin_api";
	}	
	
	@PostMapping("/authenticate")
	public String generateToken(@RequestBody AuthReq authReq) throws Exception {
		
		try {
	
			 authManager.authenticate(
					 
					 new UsernamePasswordAuthenticationToken(authReq.getUsername(),authReq.getPassword())
			);
			
		}catch(Exception e) {
			
			throw new Exception("Invalid username and password");
			
		}
	
		return jwtUtil.generateToken(authReq.getUsername());
				 
	}
	
}
